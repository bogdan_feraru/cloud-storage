TODO List:

- Improve exception handling
- create a strategy to rename the file and try again to store it
- Create a Job to delete files which are in the file system but not in the database
- Create a job to cleanup the expired token
- Improve error messages
- Write javadoc