package com.osf.homework.cloudstorage.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SearchResultItemDto {

    private String path;
}
