package com.osf.homework.cloudstorage.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoredFileDto {
    private String name;
    private String path;
}
