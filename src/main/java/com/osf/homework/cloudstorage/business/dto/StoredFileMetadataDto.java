package com.osf.homework.cloudstorage.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoredFileMetadataDto {
    private String name;
    private String path;
    private LocalDateTime timeCreated;
    private Long size;
    private String contentType;
}
