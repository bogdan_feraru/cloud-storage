package com.osf.homework.cloudstorage.business.service.impl;

import com.osf.homework.cloudstorage.business.dto.RegistrationDataDto;
import com.osf.homework.cloudstorage.business.service.UserService;
import com.osf.homework.cloudstorage.business.service.exception.UserAlreadyExistsException;
import com.osf.homework.cloudstorage.persistence.entity.User;
import com.osf.homework.cloudstorage.persistence.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@AllArgsConstructor
class DefaultMemoryUsers implements UserService {

    private UserRepository userRepository;

    @Override
    @Transactional
    public User registerNewUser(final RegistrationDataDto registrationDataDto) {
        ensureUserUniqueness(registrationDataDto);

        return userRepository.save(new User(null, registrationDataDto.getUsername(), registrationDataDto.getEmail(), registrationDataDto.getPassword()));
    }

    @Override
    public Optional<User> findById(final Long id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findByUsername(final String username) {
        return userRepository.findByUsername(username);
    }

    private void throwUserAlreadyExistsException(String message) {
        throw new UserAlreadyExistsException(message);
    }

    private void ensureUserUniqueness(RegistrationDataDto registrationDataDto) {
        userRepository.findByUsername(registrationDataDto.getUsername())
                .ifPresent(user -> throwUserAlreadyExistsException("Username not allowed!"));

        userRepository.findByEmail(registrationDataDto.getEmail())
                .ifPresent(user -> throwUserAlreadyExistsException("Email not allowed!"));
    }

}
