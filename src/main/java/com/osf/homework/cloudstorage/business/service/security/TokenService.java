package com.osf.homework.cloudstorage.business.service.security;

import com.osf.homework.cloudstorage.persistence.entity.AuthorizationToken;
import com.osf.homework.cloudstorage.persistence.entity.User;

import java.util.List;
import java.util.Optional;

public interface TokenService {

    AuthorizationToken save(AuthorizationToken authorizationToken);

    Optional<User> getUser(String tokenValue);

    List<AuthorizationToken> getAllByOwnerId(Long ownerId);

    void delete(AuthorizationToken token);

    Optional<AuthorizationToken> getByValue(String tokenValue);
}
