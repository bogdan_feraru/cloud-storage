package com.osf.homework.cloudstorage.business.service;

import com.osf.homework.cloudstorage.business.dto.RegistrationDataDto;
import com.osf.homework.cloudstorage.persistence.entity.User;

import java.util.Optional;

public interface UserService {

    User registerNewUser(RegistrationDataDto registrationDataDto);

    Optional<User> findById(Long id);

    Optional<User> findByUsername(String email);
}
