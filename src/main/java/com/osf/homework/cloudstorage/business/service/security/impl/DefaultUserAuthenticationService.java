package com.osf.homework.cloudstorage.business.service.security.impl;

import com.osf.homework.cloudstorage.business.service.UserService;
import com.osf.homework.cloudstorage.business.service.security.TokenGenerator;
import com.osf.homework.cloudstorage.business.service.security.TokenService;
import com.osf.homework.cloudstorage.business.service.security.TokenValidityProvider;
import com.osf.homework.cloudstorage.business.service.security.UserAuthenticationService;
import com.osf.homework.cloudstorage.business.service.security.model.DefaultUserDetails;
import com.osf.homework.cloudstorage.persistence.entity.AuthorizationToken;
import com.osf.homework.cloudstorage.persistence.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
class DefaultUserAuthenticationService implements UserAuthenticationService {

    private final UserService userService;

    private final TokenService tokenService;

    private final TokenGenerator tokenGenerator;

    private final TokenValidityProvider tokenValidityProvider;

    @Override
    @Transactional
    public Optional<AuthorizationToken> login(final String username, final String password) {
        final User user = userService.findByUsername(username)
                .filter(usr -> usr.getPassword().equals(password))
                .orElseThrow(() -> new RuntimeException()); //todo


        return Optional.ofNullable(createAuthorizationToken(user));
    }

    @Override
    @Transactional
    public Optional<DefaultUserDetails> findByToken(final String token) {
        return tokenService.getUser(token)
                .map(this::userToUserDetails);
    }

    private DefaultUserDetails userToUserDetails(User user) {
        return new DefaultUserDetails(user.getId(), user.getUsername(), user.getEmail(), user.getPassword());
    }

    @Override
    @Transactional
    public boolean logout(final DefaultUserDetails userDetails) {
        final List<AuthorizationToken> tokens = tokenService.getAllByOwnerId(userDetails.getId());
        if(tokens.isEmpty()) {
            return false;
        }
        tokens.forEach(this::deleteToken);
        return true;
    }

    private AuthorizationToken deleteToken(AuthorizationToken token) {
        tokenService.delete(token);
        return token;
    }

    @Override
    @Transactional
    public Optional<AuthorizationToken> loginRefresh(Long userId) {
        return Optional.ofNullable(refreshAuthorizationToken(tokenService.getAllByOwnerId(userId)));
    }

    private AuthorizationToken refreshAuthorizationToken(List<AuthorizationToken> authorizationToken) {
        User owner = authorizationToken.get(0).getOwner();
        authorizationToken
                .forEach(tokenService::delete);

        return createAuthorizationToken(owner);
    }

    private AuthorizationToken createAuthorizationToken(User user) {
        final String token = tokenGenerator.generate();
        final AuthorizationToken authorizationToken = new AuthorizationToken(null, token, user, tokenValidityProvider.getExpirationTime());
        tokenService.save(authorizationToken);

        return authorizationToken;
    }
}