package com.osf.homework.cloudstorage.business.service.security.impl;

import com.osf.homework.cloudstorage.business.service.security.TokenService;
import com.osf.homework.cloudstorage.persistence.entity.AuthorizationToken;
import com.osf.homework.cloudstorage.persistence.entity.User;
import com.osf.homework.cloudstorage.persistence.repository.AuthorizationTokenRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class DefaultTokenService implements TokenService {

    private AuthorizationTokenRepository tokenRepository;

    @Override
    public AuthorizationToken save(AuthorizationToken authorizationToken) {
        return tokenRepository.save(authorizationToken);
    }

    @Override
    @Transactional
    public Optional<User> getUser(String tokenValue) {
        return tokenRepository.findByToken(tokenValue)
                .map(AuthorizationToken::getOwner);
    }

    @Override
    @Transactional
    public List<AuthorizationToken> getAllByOwnerId(Long ownerId) {
        return tokenRepository.findAllByOwnerId(ownerId);
    }

    @Override
    @Transactional
    public void delete(AuthorizationToken token) {
        tokenRepository.delete(token);
    }

    @Override
    @Transactional
    public Optional<AuthorizationToken> getByValue(String tokenValue) {
        return tokenRepository.findByToken(tokenValue);
    }
}
