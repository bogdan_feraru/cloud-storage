package com.osf.homework.cloudstorage.business.service.impl;

import com.osf.homework.cloudstorage.business.dto.SearchResultItemDto;
import com.osf.homework.cloudstorage.business.dto.StoredFileDto;
import com.osf.homework.cloudstorage.business.dto.StoredFileMetadataDto;
import com.osf.homework.cloudstorage.business.service.FileStorageService;
import com.osf.homework.cloudstorage.business.service.exception.IllegalFileNameException;
import com.osf.homework.cloudstorage.business.service.exception.StoreException;
import com.osf.homework.cloudstorage.persistence.entity.StoredItemMetadata;
import com.osf.homework.cloudstorage.persistence.repository.FileRepository;
import com.osf.homework.cloudstorage.persistence.repository.StoredItemRepository;
import com.osf.homework.cloudstorage.persistence.repository.UserRepository;
import com.osf.homework.cloudstorage.persistence.repository.exception.FileExistsException;
import com.osf.homework.cloudstorage.persistence.repository.exception.FilePersistenceException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DefaultFileStorageService implements FileStorageService {

    private final StoredItemRepository storedItemRepository;
    private final UserRepository userRepository;
    private final FileRepository fileRepository;

    private final String storageDirectoryBase;

    public DefaultFileStorageService(StoredItemRepository storedItemRepository,
                                     UserRepository userRepository,
                                     FileRepository fileRepository,
                                     @Value("${storage.directory}") String storageDirectoryBase) {
        this.storedItemRepository = storedItemRepository;
        this.userRepository = userRepository;
        this.fileRepository = fileRepository;
        this.storageDirectoryBase = storageDirectoryBase;
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public String storeFile(Long userId, MultipartFile file) {
        final String originalFilename = stripWhitespaces(file.getOriginalFilename());
        validateFilename(originalFilename);

        Path path = storeFile(userId, file, originalFilename);
        return storeFileMetadata(userId, path);
    }

    private String storeFileMetadata(Long userId, Path path) {
        final String pathStr = path.getFileName().toString();
        userRepository.findById(userId)
                .map(user -> new StoredItemMetadata(null, pathStr, user))
                .ifPresent(storedItemRepository::save);
        return pathStr;
    }

    @Override
    @Transactional
    public List<StoredFileDto> getAllStoredFile(Long userId) {
        return storedItemRepository.getAllByOwnerId(userId).stream()
                .map(storedItemMetadata -> storeItemToUpload(storedItemMetadata, userId))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void deleteStoredFile(String uploadName, Long ownerId) {
        storedItemRepository.getByNameAndOwnerId(uploadName, ownerId)
                .ifPresent(storedItemRepository::delete);
    }

    @Override
    @Transactional
    public List<SearchResultItemDto> search(String query, Long id) {
        if(StringUtils.isEmpty(query)) {
            return Collections.emptyList();
        }

        String result = query.replace('*', '%');
        return storedItemRepository.searchByNameAndOwnerId(result, id).stream()
                .map(item -> toSearchResult(item, id))
                .collect(Collectors.toList());
    }

    private SearchResultItemDto toSearchResult(StoredItemMetadata storedItemMetadata, Long id) {
        return new SearchResultItemDto(getUserRelativePath(storedItemMetadata.getName(), id));
    }

    @Override
    @Transactional
    public StoredFileMetadataDto getStoredFileMetadata(String uploadName, Long userId) {
        final Path path = getFilePathFor(uploadName, userId);
        if (Files.exists(path)) {
            return extractStoredFileMetadata(uploadName, path);
        }

        return null;
    }

    // TODO move this to persistence layer
    @Override
    @Transactional
    public Optional<Path> getStoredFileContent(String uploadName, Long userId) {
        return storedItemRepository.getByNameAndOwnerId(uploadName, userId)
                .map(item -> getFilePathFor(uploadName, userId));
    }

    private StoredFileMetadataDto extractStoredFileMetadata(String uploadName, Path path) {
        try {
            final BasicFileAttributes basicFileAttributes = Files.readAttributes(path, BasicFileAttributes.class);
            final FileTime fileTime = basicFileAttributes.creationTime();
            final long size = basicFileAttributes.size();
            final String contentType = Files.probeContentType(path);
            return new StoredFileMetadataDto(uploadName, path.toString(), LocalDateTime.ofInstant(fileTime.toInstant(), ZoneId.systemDefault()), size, contentType);
        } catch (IOException e) {
            throw new StoreException(e);
        }
    }

    private Path getFilePathFor(String uploadName, Long userId) {
        return Paths.get(getUserDirPath(userId) + uploadName);
    }

    private StoredFileDto storeItemToUpload(StoredItemMetadata storedItemMetadata, Long userId) {
        return new StoredFileDto(storedItemMetadata.getName(), File.separator + userId + File.separator + storedItemMetadata.getName());
    }

    private String getUserDirPath(Long userId) {
        return storageDirectoryBase + File.separator + userId + File.separator;
    }

    private String getUserRelativePath(String uploadName, Long userId) {
        return File.separator + userId + File.separator + uploadName;
    }


    private Path storeFile(Long userId, MultipartFile file, String originalFilename) {
        try {
            return fileRepository.storeFile(getFilePathFor(originalFilename, userId), getFileContent(file));
        } catch (FileExistsException ex) {
            throw new StoreException("File already exists"); // TODO create a strategy to rename the file and try again to store it
        } catch (FilePersistenceException ex) {
            throw new StoreException(ex);
        }
    }

    private byte[] getFileContent(MultipartFile file) {
        try {
            return file.getBytes();
        } catch (IOException e) {
            throw new StoreException("Error occured when tried to fetch the content of the file");
        }
    }

    private void validateFilename(String originalFilename) {
        if (originalFilename == null) {
            throw new IllegalFileNameException();
        }
    }

    private String stripWhitespaces(String originalFilename) {
        if (StringUtils.isEmpty(originalFilename)) {
            return null;
        }

        return originalFilename.replaceAll("\\s+", "");
    }
}
