package com.osf.homework.cloudstorage.business.dto;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class RegistrationDataDto {

    String username;
    String email;
    String password;
}
