package com.osf.homework.cloudstorage.business.service.security;

import com.osf.homework.cloudstorage.business.service.security.model.DefaultUserDetails;
import com.osf.homework.cloudstorage.persistence.entity.AuthorizationToken;

import java.util.List;
import java.util.Optional;

public interface UserAuthenticationService {

    Optional<AuthorizationToken> login(String username, String password);

    Optional<DefaultUserDetails> findByToken(String token);

    boolean logout(DefaultUserDetails userDetails);

    Optional<AuthorizationToken> loginRefresh(Long userId);
}
