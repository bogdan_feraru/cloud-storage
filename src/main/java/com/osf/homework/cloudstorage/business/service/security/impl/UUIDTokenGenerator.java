package com.osf.homework.cloudstorage.business.service.security.impl;

import com.osf.homework.cloudstorage.business.service.security.TokenGenerator;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UUIDTokenGenerator implements TokenGenerator {

    @Override
    public String generate() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
