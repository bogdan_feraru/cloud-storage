package com.osf.homework.cloudstorage.business.service.security;

public interface TokenGenerator {

    String generate();
}
