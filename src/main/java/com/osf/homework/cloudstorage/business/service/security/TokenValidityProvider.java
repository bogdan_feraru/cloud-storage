package com.osf.homework.cloudstorage.business.service.security;

import java.time.Duration;
import java.time.LocalDateTime;

public interface TokenValidityProvider {

    LocalDateTime getExpirationTime();

    Duration getValidityDuration();
}
