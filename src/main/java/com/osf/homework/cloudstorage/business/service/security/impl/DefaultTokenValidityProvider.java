package com.osf.homework.cloudstorage.business.service.security.impl;

import com.osf.homework.cloudstorage.business.service.security.TokenValidityProvider;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;

@Component
public class DefaultTokenValidityProvider implements TokenValidityProvider {

    private static final int DEFAULT_TOKEN_VALIDITY_IN_MINUTES = 30;

    private final Duration tokenValidity;

    public DefaultTokenValidityProvider() {
        this.tokenValidity = Duration.ofMinutes(DEFAULT_TOKEN_VALIDITY_IN_MINUTES);
    }

    public DefaultTokenValidityProvider(Duration tokenValidity) {
        this.tokenValidity = tokenValidity;
    }

    @Override
    public LocalDateTime getExpirationTime() {
        return LocalDateTime.now().plus(tokenValidity);
    }

    @Override
    public Duration getValidityDuration() {
        return tokenValidity;
    }
}
