package com.osf.homework.cloudstorage.business.service.exception;

public class IllegalFileNameException extends ServiceException {

    public IllegalFileNameException() {
    }

    public IllegalFileNameException(String message) {
        super(message);
    }

    public IllegalFileNameException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalFileNameException(Throwable cause) {
        super(cause);
    }

    public IllegalFileNameException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
