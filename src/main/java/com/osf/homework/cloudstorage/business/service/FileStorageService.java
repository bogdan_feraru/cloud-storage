package com.osf.homework.cloudstorage.business.service;

import com.osf.homework.cloudstorage.business.dto.SearchResultItemDto;
import com.osf.homework.cloudstorage.business.dto.StoredFileDto;
import com.osf.homework.cloudstorage.business.dto.StoredFileMetadataDto;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

public interface FileStorageService {

    String storeFile(Long userId, MultipartFile file);

    List<StoredFileDto> getAllStoredFile(Long userId);

    void deleteStoredFile(String uploadName, Long ownerId);

    List<SearchResultItemDto> search(String query, Long id);

    StoredFileMetadataDto getStoredFileMetadata(String uploadName, Long userId);

    Optional<Path> getStoredFileContent(String uploadName, Long userId);
}
