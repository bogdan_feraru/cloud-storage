package com.osf.homework.cloudstorage.api.viewmodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoredFileResponse {

    private String message;
    private StoredFile object;
}
