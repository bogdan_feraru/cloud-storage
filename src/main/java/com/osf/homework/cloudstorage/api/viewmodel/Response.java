package com.osf.homework.cloudstorage.api.viewmodel;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Response {

    private Status status;
    private String message;

    public static Response success() {
        return new Response(Status.SUCCESS, null);
    }

    public static Response error(String message) {
        return new Response(Status.ERROR, message);
    }
}
