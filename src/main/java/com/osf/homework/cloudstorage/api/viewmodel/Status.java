package com.osf.homework.cloudstorage.api.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Status {

    @JsonProperty("success")
    SUCCESS,

    @JsonProperty("error")
    ERROR
}
