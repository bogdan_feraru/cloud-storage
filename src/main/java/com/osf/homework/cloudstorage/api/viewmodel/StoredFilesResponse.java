package com.osf.homework.cloudstorage.api.viewmodel;

import com.osf.homework.cloudstorage.business.dto.StoredFileDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoredFilesResponse {

    private Status message;

    private List<StoredFileDto> objects;
}
