package com.osf.homework.cloudstorage.api.controller;

import com.osf.homework.cloudstorage.api.viewmodel.Response;
import com.osf.homework.cloudstorage.api.viewmodel.SearchResult;
import com.osf.homework.cloudstorage.api.viewmodel.Status;
import com.osf.homework.cloudstorage.api.viewmodel.StoredFile;
import com.osf.homework.cloudstorage.api.viewmodel.StoredFileResponse;
import com.osf.homework.cloudstorage.api.viewmodel.StoredFilesResponse;
import com.osf.homework.cloudstorage.business.dto.SearchResultItemDto;
import com.osf.homework.cloudstorage.business.dto.StoredFileDto;
import com.osf.homework.cloudstorage.business.service.FileStorageService;
import com.osf.homework.cloudstorage.business.service.exception.IllegalFileNameException;
import com.osf.homework.cloudstorage.business.service.exception.StoreException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;

@RestController
public class StoredFileController extends AbstractController {

    private static final String MEDIA_CONTENT = "media";
    private static final String STORE_EXCEPTION_MESSAGE = "An internal error occured when tried to store the file";
    private static final String ILLEGAL_FILENAME_MESSAGE = "The name of the file is not valid.";
    private FileStorageService fileStorageService;

    public StoredFileController(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }

    @PostMapping("/upload")
    public StoredFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = fileStorageService.storeFile(getCurrentlyLoggedUserId(), file);
        return new StoredFileResponse("success", new StoredFile(fileName));
    }

    @GetMapping("/list")
    public StoredFilesResponse getUploadedFiles() {
        List<StoredFileDto> storedFileDtoList = fileStorageService.getAllStoredFile(getCurrentlyLoggedUserId());
        return new StoredFilesResponse(Status.SUCCESS, storedFileDtoList);
    }

    @DeleteMapping("/{uploadName}")
    public Response deleteUpload(@PathVariable("uploadName") String uploadName) {
        fileStorageService.deleteStoredFile(uploadName, getCurrentlyLoggedUserId());
        return Response.success();
    }

    @PostMapping("/find")
    public SearchResult find(@RequestParam("name") String query) {
        List<SearchResultItemDto> results = fileStorageService.search(query, getCurrentlyLoggedUserId());
        return new SearchResult(Status.SUCCESS, results);
    }

    @GetMapping("/{uploadName}")
    public ResponseEntity<?> getUpload(@PathVariable("uploadName") String uploadName, @RequestParam(value = "alt", required = false) String alt) {
        if (MEDIA_CONTENT.equals(alt)) {
            return fileStorageService.getStoredFileContent(uploadName, getCurrentlyLoggedUserId())
                    .map(this::successResponse)
                    .orElse(ResponseEntity.notFound().build());
        }

        return ResponseEntity.ok(fileStorageService.getStoredFileMetadata(uploadName, getCurrentlyLoggedUserId()));
    }

    @ExceptionHandler(StoreException.class)
    public ResponseEntity<?> handleStoreException(StoreException ex) {
        final String message = ex.getMessage();
        return ResponseEntity.ok(Response.error(StringUtils.isEmpty(message) ? STORE_EXCEPTION_MESSAGE : message));
    }

    @ExceptionHandler(IllegalFileNameException.class)
    public ResponseEntity<?> handleIllegalFileNameException(IllegalFileNameException ex) {
        final String message = ex.getMessage();
        return ResponseEntity.ok(Response.error(StringUtils.isEmpty(message) ? ILLEGAL_FILENAME_MESSAGE : message));
    }

    private ResponseEntity<?> successResponse(Path path) {
        return ResponseEntity.status(HttpStatus.FOUND)
                .header("Content-Disposition", "attachment; filename=" + path.toString())
                .body(new FileSystemResource(path));
    }
}
