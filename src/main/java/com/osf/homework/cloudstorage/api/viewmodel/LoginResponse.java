package com.osf.homework.cloudstorage.api.viewmodel;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LoginResponse {

    private Status status;
    private String token;
    private String message;
    private LocalDateTime expiresAt;

    public static LoginResponse loginSuccessful(String token, LocalDateTime expiresAt) {
        return new LoginResponse(Status.SUCCESS, token, null, expiresAt);
    }

    public static LoginResponse loginUnsuccessful(String message) {
        return new LoginResponse(Status.ERROR, null, message, null);
    }
}
