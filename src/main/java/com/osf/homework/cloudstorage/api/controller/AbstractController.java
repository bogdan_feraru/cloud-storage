package com.osf.homework.cloudstorage.api.controller;

import com.osf.homework.cloudstorage.business.service.security.model.DefaultUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class AbstractController {

    protected static DefaultUserDetails getCurrentlyLoggedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (DefaultUserDetails) authentication.getPrincipal();
    }

    protected static Long getCurrentlyLoggedUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final DefaultUserDetails userDetails = (DefaultUserDetails) authentication.getPrincipal();
        if (userDetails != null) {
            return userDetails.getId();
        }

        return null;
    }
}
