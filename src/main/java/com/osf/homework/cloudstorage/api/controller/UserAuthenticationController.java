package com.osf.homework.cloudstorage.api.controller;

import com.osf.homework.cloudstorage.api.viewmodel.LoginForm;
import com.osf.homework.cloudstorage.api.viewmodel.LoginResponse;
import com.osf.homework.cloudstorage.api.viewmodel.Response;
import com.osf.homework.cloudstorage.business.dto.RegistrationDataDto;
import com.osf.homework.cloudstorage.business.service.UserService;
import com.osf.homework.cloudstorage.business.service.exception.UserAlreadyExistsException;
import com.osf.homework.cloudstorage.business.service.security.UserAuthenticationService;
import com.osf.homework.cloudstorage.business.service.security.model.DefaultUserDetails;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class UserAuthenticationController extends AbstractController {

    @NonNull
    private UserAuthenticationService authenticationService;

    @NonNull
    private UserService userService;

    @PostMapping("/signup")
    public Response register(@RequestBody RegistrationDataDto registrationDataDto) {
        userService.registerNewUser(registrationDataDto);

        return Response.success();
    }

    @PostMapping("/login")
    public LoginResponse login(@RequestBody LoginForm loginForm) {
        return authenticationService
                .login(loginForm.getUsername(), loginForm.getPassword())
                .map(token -> LoginResponse.loginSuccessful(token.getToken(), token.getExpiresAt()))
                .orElseGet(() -> LoginResponse.loginUnsuccessful("Wrong username or password"));
    }

    @PostMapping("/login/refresh")
    public LoginResponse loginRefresh() {
        return authenticationService
                .loginRefresh(getCurrentlyLoggedUserId())
                .map(token -> LoginResponse.loginSuccessful(token.getToken(), token.getExpiresAt()))
                .orElseGet(() -> LoginResponse.loginUnsuccessful("Token expired"));
    }

    // TODO complete implementation
    @PostMapping("/logout")
    public Response logout(@AuthenticationPrincipal final DefaultUserDetails userDetails) {
        final boolean logout = authenticationService.logout(userDetails);
        if(logout) {
            return Response.success();
        }
        return Response.error("Token expired");
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<?> handleUserAlreadyExistsException(UserAlreadyExistsException ex) {
        return ResponseEntity.ok(Response.error(ex.getMessage()));
    }
}