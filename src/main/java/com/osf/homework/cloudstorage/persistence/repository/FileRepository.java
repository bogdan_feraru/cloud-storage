package com.osf.homework.cloudstorage.persistence.repository;

import java.nio.file.Path;

public interface FileRepository {

    Path storeFile(Path path, byte[] fileContent);
}
