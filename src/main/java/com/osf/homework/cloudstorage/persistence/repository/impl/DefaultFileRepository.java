package com.osf.homework.cloudstorage.persistence.repository.impl;

import com.osf.homework.cloudstorage.persistence.repository.FileRepository;
import com.osf.homework.cloudstorage.persistence.repository.exception.FileExistsException;
import com.osf.homework.cloudstorage.persistence.repository.exception.FilePersistenceException;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Repository
public class DefaultFileRepository implements FileRepository {

    @Override
    public Path storeFile(Path path, byte[] fileContent) {
        final Path absolutePath = path.toAbsolutePath();
        if (!Files.exists(absolutePath.getParent())) {
            createSubdirectories(absolutePath);
        }

        if (Files.exists(absolutePath)) {
            throw new FileExistsException();
        }

        createFile(absolutePath);
        writeFile(path, fileContent);

        return path;
    }

    private void writeFile(Path path, byte[] fileContent) {
        try {
            Files.write(path, fileContent);
        } catch (IOException e) {
            throw new FilePersistenceException(e);
        }
    }

    private void createFile(Path absolutePath) {
        try {
            Files.createFile(absolutePath);
        } catch (IOException e) {
            throw new FilePersistenceException(e);
        }
    }

    private void createSubdirectories(Path absolutePath) {
        try {
            Files.createDirectories(absolutePath.getParent());
        } catch (IOException e) {
            throw new FilePersistenceException(e);
        }
    }
}
