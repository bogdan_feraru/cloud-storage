package com.osf.homework.cloudstorage.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "authorization_token")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthorizationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String token;

    @ManyToOne(fetch = FetchType.LAZY)
    private User owner;

    @Column(name = "expires_at")
    LocalDateTime expiresAt;
}
