package com.osf.homework.cloudstorage.persistence.repository;

import com.osf.homework.cloudstorage.persistence.entity.StoredItemMetadata;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface StoredItemRepository extends CrudRepository<StoredItemMetadata, Long> {

    List<StoredItemMetadata> getAllByOwnerId(Long ownerId);

    Optional<StoredItemMetadata> getByNameAndOwnerId(String name, Long ownerId);

    @Query("SELECT so FROM StoredItemMetadata so WHERE so.owner.id=:ownerId AND name like :query")
    List<StoredItemMetadata> searchByNameAndOwnerId(@Param("query") String query, @Param("ownerId") Long ownerId);
}
