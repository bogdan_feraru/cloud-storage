package com.osf.homework.cloudstorage.persistence.repository.exception;

public class FilePersistenceException extends PersistenceException {

    public FilePersistenceException() {
    }

    public FilePersistenceException(String message) {
        super(message);
    }

    public FilePersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public FilePersistenceException(Throwable cause) {
        super(cause);
    }

    public FilePersistenceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
