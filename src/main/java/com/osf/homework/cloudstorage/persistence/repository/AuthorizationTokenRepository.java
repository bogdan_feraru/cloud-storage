package com.osf.homework.cloudstorage.persistence.repository;

import com.osf.homework.cloudstorage.persistence.entity.AuthorizationToken;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AuthorizationTokenRepository extends CrudRepository<AuthorizationToken, Long> {

    Optional<AuthorizationToken> findByToken(String token);

    List<AuthorizationToken> findAllByOwnerId(Long ownerId);
}
